<?php return array (
  'components' => 
  array (
    'db' => 
    array (
      'class' => 'yii\\db\\Connection',
      'dsn' => 'mysql:host=localhost;dbname=readers',
      'username' => 'root',
      'password' => 'root',
      'charset' => 'utf8',
    ),
    'cache' => 
    array (
      'class' => 'yii\\caching\\FileCache',
      'keyPrefix' => 'humhub',
    ),
    'user' => 
    array (
    ),
    'mailer' => 
    array (
      'transport' => 
      array (
        'class' => 'Swift_MailTransport',
      ),
    ),
    'formatter' => 
    array (
      'defaultTimeZone' => 'America/Sao_Paulo',
    ),
    'formatterApp' => 
    array (
      'defaultTimeZone' => 'America/Sao_Paulo',
      'timeZone' => 'America/Sao_Paulo',
    ),
  ),
  'params' => 
  array (
    'installer' => 
    array (
      'db' => 
      array (
        'installer_hostname' => 'localhost',
        'installer_database' => 'readers',
      ),
    ),
    'config_created_at' => 1541008292,
    'horImageScrollOnMobile' => '1',
    'databaseInstalled' => true,
    'installed' => true,
  ),
  'name' => 'Readers',
  'language' => 'pt_br',
  'timeZone' => 'America/Sao_Paulo',
); ?>